package com.epam;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        final int times_count = 100;
        int times_rumor_spreaded_count = 0;
        int people_rumor_heard_count = 0;
        boolean person_already_heard = false;
        int random_next_person = -1;
        int current_person = 1;
        Scanner sc = new Scanner(System.in);
        System.out.println("Amount of people at the party: ");
        int n = sc.nextInt();
        for(int i=0;i<times_count;i++){
            boolean guests[] = new boolean[n];
            guests[1] = true;
            while(!person_already_heard){
                random_next_person = 1 + (int)(Math.random() * (n-1));
                if(random_next_person == current_person){
                    while(random_next_person == current_person)
                        random_next_person = 1 + (int)(Math.random() * (n-1));
                }
                if(guests[random_next_person])
                {
                    if(rumor_spreaded_finally(guests))
                        times_rumor_spreaded_count++;
                    people_rumor_heard_count = people_rumor_heard_count + count_people_already_heard(guests);
                    person_already_heard = true;
                }
                guests[random_next_person] = true;
                current_person = random_next_person;
            }
        }
        System.out.println("Probability that everyone at the party (except Alice) will hear the rumor before it stops propagating: " +
                (double)times_rumor_spreaded_count/times_count);
        System.out.println("An estimate of the expected number of people to hear the rumor: " + people_rumor_heard_count/times_count);
    }

    public static int count_people_already_heard(boolean array[]){
        int count = 0;
        for(int i = 1;i<array.length;i++)
            if(array[i])
                count++;
        return count;
    }

    public static boolean rumor_spreaded_finally(boolean array[]){
        for(int i = 1;i<array.length;i++)
            if(!array[i])
                return false;
        return true;
    }
}
